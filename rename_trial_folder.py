import os

def remove_trial_suffix_from_folders():
    current_folder = os.getcwd()
    for folder_name in os.listdir(current_folder):
        if os.path.isdir(folder_name) and folder_name.endswith("_trial"):
            new_folder_name = folder_name[:-len("_trial")]
            os.rename(folder_name, new_folder_name)
            print(f"Renamed '{folder_name}' to '{new_folder_name}'")

if __name__ == "__main__":
    remove_trial_suffix_from_folders()