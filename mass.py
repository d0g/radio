import os
from pydub import AudioSegment

# Function to convert a single .wav file to .mp3 format
def convert_wav_to_mp3(file):
    # Load the .wav file
    audio = AudioSegment.from_wav(file)

    # Get the file name without the extension
    file_name = os.path.splitext(file)[0]

    # Export to .mp3 format
    audio.export(file_name + ".mp3", format="mp3")

    print("Conversion of", file, "to MP3 format complete. New file saved as", file_name + ".mp3")

# Function to traverse directories recursively and convert .wav files
def traverse_directories(directory):
    # Iterate over all items in the directory
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)

        # If it's a directory, recursively call the function
        if os.path.isdir(item_path):
            traverse_directories(item_path)

        # If it's a .wav file, convert it to .mp3 format
        if item.endswith(".wav"):
            convert_wav_to_mp3(item_path)

# Function to remove .wav files
def remove_wav_files(directory):
    # Iterate over all items in the directory
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)

        # If it's a directory, recursively call the function
        if os.path.isdir(item_path):
            remove_wav_files(item_path)

        # If it's a .wav file, remove it
        if item.endswith(".wav"):
            os.remove(item_path)

# Get the current directory
current_dir = os.getcwd()

# Start traversing directories recursively from the current directory
traverse_directories(current_dir)

print("Conversion of .wav files to .mp3 format in all subdirectories complete.")

# Remove .wav files in all subdirectories
remove_wav_files(current_dir)

print("Removal of .wav files in all subdirectories complete.")
