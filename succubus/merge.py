import os
import json

def process_json_file(file_path, genre_folder):
    with open(file_path, "r", encoding="utf-8") as json_file:
        data = json.load(json_file)
        files = []
        poster = ""
        mp3_files = []
        for mp3_file in os.listdir(os.path.dirname(file_path)):
            if mp3_file.endswith(".mp3"):
                mp3_files.append(mp3_file)
            if mp3_file.endswith(".jpg") or mp3_file.endswith(".png") or mp3_file.endswith(".webp"):
                poster = f"https://radio.merahputih.moe/{genre_folder}/{os.path.basename(os.path.dirname(file_path))}/{mp3_file}"
        
        mp3_files.sort(key=lambda x: int(os.path.splitext(x)[0]))  # Sort the mp3 files numerically
        for mp3_file in mp3_files:
            file_path = f"https://gitlab.com/d0g/radio/-/raw/master/{genre_folder}/{os.path.basename(os.path.dirname(file_path))}/{mp3_file}"
            files.append(file_path)

        data["files"] = files
        data["poster"] = poster
        
        return data

def traverse_directories(directory, genre_folder):
    all_data = []
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            data = traverse_directories(item_path, genre_folder)
            if data:
                all_data.extend(data)
        if item == "data.json":
            data = process_json_file(item_path, genre_folder)
            if data:
                all_data.append(data)
    return all_data

root_dir = os.getcwd()
genre_folder = os.path.basename(root_dir)
all_data = traverse_directories(root_dir, genre_folder)
output_file = os.path.join(root_dir, "all.json")
with open(output_file, "w", encoding="utf-8") as outfile:
    json.dump(all_data, outfile, indent=4, ensure_ascii=False)

print("Data from all 'data.json' files has been updated with the 'files' array and saved as 'all.json'.")
