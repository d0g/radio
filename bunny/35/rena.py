import os
import shutil
from pathlib import Path

def auto_rename_wav_mp3_files(folder_path):
    folder_path = Path(folder_path)
    if not folder_path.is_dir():
        print("Invalid folder path.")
        return

    audio_extensions = ['.wav', '.mp3']
    audio_files = [file for file in folder_path.iterdir() if file.suffix.lower() in audio_extensions]

    for index, audio_file in enumerate(audio_files, start=1):
        new_file_name = f"{index}{audio_file.suffix}"
        new_file_path = folder_path.joinpath(new_file_name)

        while new_file_path.exists():
            index += 1
            new_file_name = f"{index}{audio_file.suffix}"
            new_file_path = folder_path.joinpath(new_file_name)

        shutil.move(str(audio_file), str(new_file_path))
        print(f"Renamed '{audio_file.name}' to '{new_file_path.name}'")

if __name__ == "__main__":
    current_folder = os.getcwd()
    auto_rename_wav_mp3_files(current_folder)
