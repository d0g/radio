from pydub import AudioSegment

# Load the "music.wav" file
audio = AudioSegment.from_wav("music.wav")

# Convert to MP3 format
audio.export("music.mp3", format="mp3")

print("Conversion to MP3 format complete. New file saved as 'music.mp3'.")